//
// Created by Quentin Glangetas on 25/09/2023.
//

#pragma once

#include <vector>
#include "../CelestialObject.h"

class ForceCalculator {
public:
    explicit ForceCalculator(std::vector<CelestialObject>& celObjects) : celObjects(celObjects) {}
    virtual ~ForceCalculator() = default;
    virtual Vec2d calculateForce(CelestialObject& mainCelObject) = 0;
protected:
    std::vector<CelestialObject>& celObjects;
};
