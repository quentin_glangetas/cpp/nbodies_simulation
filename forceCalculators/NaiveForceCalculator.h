//
// Created by Quentin Glangetas on 25/09/2023.
//

#pragma once

#include <ctgmath>
#include "ForceCalculator.h"
#include "../constants.h"

class NaiveForceCalculator : public ForceCalculator {
public:
    explicit NaiveForceCalculator(std::vector<CelestialObject>& celObjects) : ForceCalculator(celObjects) {}
    ~NaiveForceCalculator() override = default;
private:
    Vec2d calculateForce(CelestialObject &mainCelObject) override;
};
