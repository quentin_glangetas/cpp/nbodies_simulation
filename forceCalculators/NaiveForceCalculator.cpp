//
// Created by Quentin Glangetas on 25/09/2023.
//

#include "NaiveForceCalculator.h"

Vec2d NaiveForceCalculator::calculateForce(CelestialObject &mainCelObject) {
    Vec2d force(0, 0);
    double distance;
    Vec2d vec;

    for (const CelestialObject& otherObject : celObjects) {
        if (mainCelObject.getPosition() == otherObject.getPosition())
            continue;

        vec = mainCelObject.getVectorTowards(otherObject);
        distance = vec.getNorm();
        vec *= mainCelObject.getMass() * otherObject.getMass() * consts::G / (distance * distance * distance);
        force += vec;
    }
    return force;
}
