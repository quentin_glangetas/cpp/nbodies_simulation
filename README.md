# N-Bodies simulation

This is my first medium-sized project in C++. Goal is to simulate N celestial objects using Newtonian gravity. 

What I've used in this project:
- Classic data structures
- Operators overload for a Vec2d data structure
- Strategy design pattern for the logs and force calculation components
- Use Clion profiler to detect performance bottlenecks
- Function calls are expensive when used many many times


# Results

## Performance Recap

If the goal is to maintain a 60 frames/second real-time simulation, here are the benchmarks: 
 
### Results A - Unoptimized simulation
<img src="output/results1_20231021.png"/>

After profiling it seems a significant amount of time is spent creating Vec2d objects. To improve performance, we need to reduce the number of Vec2d being created during operation overloads and rotation, etc

### Results B - Optimizing Vec2d operations (+27% improvement from A)
<img src="output/results2_20231022.png"/>

Reduced the number of constructor called during `Vec2d` operations. E.g. in `Vec2d::getUnitVectorTowards(const Vec2d&)` uses `vec /= vec.getNorm()` rather than `return vec / vec.getNorm()`. This reduces the call to constructor by one for every call of `getUnitVectorTowards`. 

Profiling still hints to the performance bottleneck being the computation of `CelestialObject::getUnitVectorTowards()`. 

### Results C - Optimizing force calculation (+65% improvement from A, +29% improvement from B)
<img src="output/results3_20231022.png"/>

Optimized `NaiveForceCalculator::calculateForce()` by reducing the number of call to `Vec2d::getNorm()`. This was called twice: once in the function call explicitly, and once more implicitly in `Vec2d::getUnitVectorTowards(const Vec2d&)`.

Now uses `Vec2d::operator*=()` and exponentiate the distance manually to the 3rd power. This improved performance substantially.

Profiling again hints to the performance bottleneck being the computation of `CelestialObject::getVectorTowards()`, but much less so. Future work would be needed to speed up that function. 




## First iteration - 4 planets solar system
<img src="output/SolarSystem.gif" width="600" height="600" />


## Second iteration - Adding Juptiter and the Asteroid belt
<img src="output/SolarSystemWithAsteroids.gif" width="600" height="600" />


# Dev Log

## Initial ambitions

The initial ambition was to generate and then simulate an entire galaxy. However, there was several problems to it:

### Problem A - Double's precision

Using the Galaxy Center as the origin caused issues. Computing distance between 2 planets in the same solar system (1e9-1e15 meters) was not accurate, i.e. was calculated as 0, using the distance from the Galaxy Center (1e36-1e40 meters). **This is due to the double precision**. 

Three potential solutions
1. **Unit change**: I thought a unit change from meters to astronomical unit or something large could solve the problem. However after getting a refreshed on double implementation into a mantissa and an exponent parts, **this proved to be ineffective**.
2. **BigDouble library**: One way to get around the precision restrictions is to use BigDouble libraries, such as `ttmath`. It worked well in practice, but was bound to make the **execution slower**
3. **Relative position vectors**: Instead of having the object track its absolute position to the Galaxy Center, it would have been possible to only track its position relative to the object its orbiting around. Coupled with a tree-like structure of the galaxy objects, we could have avoided the problem of different order of magnitude by using relative positions when calculating distance. **However, this only exercebated Problem B**. 


### Problem B - Complex maths

The Newtonian gravitational maths proved to be quite complicated when considering different objects in orbit of each other. It is easy to build a random unstable system, but much harder to generate a coherent galaxy where planets orbit stars, and stars orbit a blackhole. 

While I could have used a already-made solution on the internet, I prefer to understand my solution fully and hence had to mostly re-arrange my project to make it realistic. 

### Deiciding on realistic ambitions

In the end, I decided to narrow down the project in simulating only the Solar System. I could still simulate large number of objects, so that I can improve my code optimization skills, but limit the maths needed to make a sustainable gravitational system. 





