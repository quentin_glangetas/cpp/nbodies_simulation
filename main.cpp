#include <iostream>
#include <string>
#include <vector>
#include "CelestialObject.h"
#include "forceCalculators/NaiveForceCalculator.h"
#include "Simulation.h"
#include "logs/ConsoleSimulationLog.h"
#include "logs/CSVSimulationLog.h"
#include "logs/TimerLog.h"
#include "logs/MultiSimulationLog.h"
#include "SolarSystemGenerator.h"



int main()
{

    SolarSystemGenerator solarSystemGenerator(764, 5);
    std::vector<CelestialObject> celObjects = solarSystemGenerator.getCelObjects();

    std::unique_ptr<ForceCalculator> fc = std::make_unique<NaiveForceCalculator>(celObjects);

    // Logging
    std::string filePath = "/Users/glangq/dev/celestial_simulation/output/celestial_simulation_log.csv";
    auto log = MultiSimulationLog();
//    log.addLog(new ConsoleSimulationLog());
    log.addLog(new TimerLog());
    log.addLog(new CSVSimulationLog(filePath));

    std::cout << celObjects.size() << " objects" << std::endl;

    Simulation simulation(celObjects, *fc.release(), log);
    simulation.simulate(2000, 60 * 60 * 24);


    return 0;
}
