//
// Created by Quentin Glangetas on 21/10/2023.
//

#include "SolarSystemGenerator.h"

SolarSystemGenerator::SolarSystemGenerator(int nAsteroids, int nPlanets)
: celObjects(),
  randomGenerator(std::random_device()())
{

    if (nPlanets <= 0 || nPlanets > 8)
        throw std::runtime_error("SolarSystem can only simulate at most 8 planets");


    static CelestialObject sun = {2e30, {0, 0}, {0, 0}, CelestialType::STAR};
    static CelestialObject mercury = {3.3e23, Vec2d(53e9, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject venus = {4.87e24, Vec2d(108e9, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject earth = {5.97e24, Vec2d(149.54e9, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject mars = {6.42e23, Vec2d(239e9, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject jupiter = {1.9e27, Vec2d(780e9, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject saturn = {5.7e26, Vec2d(1.4e12, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject uranus = {8.6e25, Vec2d(2.9e12, 0), {0, 0}, CelestialType::PLANET};
    static CelestialObject neptune = {1e26, Vec2d(4.5e12, 0), {0, 0}, CelestialType::PLANET};

    mercury.makeObjectOrbitAround(sun);
    venus.makeObjectOrbitAround(sun);
    earth.makeObjectOrbitAround(sun);
    mars.makeObjectOrbitAround(sun);
    jupiter.makeObjectOrbitAround(sun);
    saturn.makeObjectOrbitAround(sun);
    uranus.makeObjectOrbitAround(sun);
    neptune.makeObjectOrbitAround(sun);

    // SUN
    celObjects.emplace_back(sun);

    // PLANETS
    std::vector<CelestialObject> planets = {mercury, venus, earth, mars, jupiter, saturn, uranus, neptune};
    for (int i = 0; i < nPlanets; i++) {
        celObjects.emplace_back(planets[i]);
    }

    // ASTEROID BELT
    for (int i = 0; i < nAsteroids; i++) {
        CelestialObject asteroid = generateAsteroid();
        asteroid.makeObjectOrbitAround(sun);
        celObjects.emplace_back(asteroid);
    }

}

const std::vector<CelestialObject> &SolarSystemGenerator::getCelObjects() const {
    return celObjects;
}

CelestialObject SolarSystemGenerator::generateAsteroid() const {
    return {generateAsteroidMass(), generateAsteroidPosition(), Vec2d(), ASTEROID};
}

Vec2d SolarSystemGenerator::generateAsteroidPosition() const {
    return Vec2d(generateAsteroidDistance(), 0).rotate(generateAngle());
}

double SolarSystemGenerator::generateAsteroidDistance() const {
    static DistanceDistribution distanceDistribution(550e9, 650e9);
    return distanceDistribution(randomGenerator);
}

double SolarSystemGenerator::generateAsteroidMass() const {
    static MassDistribution asteroidMassDistribution(1e15, 1e19);
    return asteroidMassDistribution(randomGenerator);
}

double SolarSystemGenerator::generateAngle() const {
    static AngleDistribution angleDistribution(0, 2 * consts::PI);
    return angleDistribution(randomGenerator);
}
