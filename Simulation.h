//
// Created by Quentin Glangetas on 25/09/2023.
//

#pragma once

#include <vector>
#include <iostream>

#include "forceCalculators/ForceCalculator.h"
#include "logs/SimulationLog.h"
#include "CelestialObject.h"

class Simulation {
public:
    Simulation(std::vector<CelestialObject>& celObjects, ForceCalculator& fc, SimulationLog& log);
    void simulate(long long nIter, double timeIncrement);
private:
    std::vector<CelestialObject>& celObjects;
    ForceCalculator& fc;
    SimulationLog& log;
    void doIter(double timeIncrement);
};
