//
// Created by Quentin Glangetas on 25/09/2023.
//

#include "CelestialObject.h"


void CelestialObject::updateObject(Vec2d &force, double dt) {
    speed += force / mass * dt;
    position += speed * dt;
}


Vec2d CelestialObject::getUnitVectorTowards(const CelestialObject &destination) const {
    return position.getUnitVectorTowards(destination.position);
}

Vec2d CelestialObject::getUnitVectorFrom(const CelestialObject &source) const {
    return position.getUnitVectorFrom(source.position);
}

void CelestialObject::makeObjectOrbitAround(const CelestialObject &massiveObject, const bool isClockwise) {
    const double scalingWrtRefObject = sqrt(consts::G * massiveObject.mass / position.getNorm());
    speed = getUnitVectorTowards(massiveObject).getOrthogonalVector(isClockwise) * scalingWrtRefObject;

}

CelestialType CelestialObject::getType() const {
    return type;
}

double CelestialObject::getMass() const {
    return mass;
}

const Vec2d &CelestialObject::getPosition() const {
    return position;
}

const Vec2d &CelestialObject::getSpeed() const {
    return speed;
}

Vec2d CelestialObject::getVectorTowards(const CelestialObject &destination) const {
    return position.getVectorTowards(destination.position);
}

Vec2d CelestialObject::getVectorFrom(const CelestialObject &source) const {
    return position.getVectorFrom(source.position);
}
