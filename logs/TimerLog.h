//
// Created by Quentin Glangetas on 21/10/2023.
//

#pragma once

#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include "SimulationLog.h"
#include "../statistics.h"

using timePoint = std::chrono::steady_clock::time_point;

class TimerLog : public SimulationLog {
public:
    TimerLog() : hasClockStarted(false), previousIterTime(), iterTimes() {}
    ~TimerLog() override = default;
    void logInitialization(const std::vector<CelestialObject> &celObjects) override;
    void logResult(const std::vector<CelestialObject> &celObjects) override;
    void logIter(const std::vector<CelestialObject> &celObjects) override;
    void logObject(const CelestialObject &celObject) override;
private:
    bool hasClockStarted;
    timePoint previousIterTime;
    std::vector<double> iterTimes;
};
