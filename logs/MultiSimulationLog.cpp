//
// Created by Quentin Glangetas on 01/10/2023.
//

#include "MultiSimulationLog.h"

MultiSimulationLog::MultiSimulationLog(std::vector<SimulationLog> &simulationLogs) {
    for (auto& log: simulationLogs) {
        logs.push_back(&log);
    }
}

void MultiSimulationLog::logInitialization(const std::vector<CelestialObject> &celObjects) {
    for (auto& log : logs)
        log->logInitialization(celObjects);
}

void MultiSimulationLog::logResult(const std::vector<CelestialObject> &celObjects) {
    for (auto& log : logs)
        log->logResult(celObjects);
}

void MultiSimulationLog::logIter(const std::vector<CelestialObject> &celObjects) {
    for (auto& log : logs)
        log->logIter(celObjects);
}

void MultiSimulationLog::logObject(const CelestialObject &celObject) {
    for (auto& log : logs)
        log->logObject(celObject);
}

MultiSimulationLog::~MultiSimulationLog() {
    for (auto* log : logs)
        delete log;
}
