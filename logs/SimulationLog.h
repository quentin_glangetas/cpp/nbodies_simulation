//
// Created by Quentin Glangetas on 01/10/2023.
//

#pragma once

#include <vector>
#include "../CelestialObject.h"


class SimulationLog {
public:
    SimulationLog() = default;
    virtual ~SimulationLog() = default;
    virtual void logInitialization(const std::vector<CelestialObject>& celObjects) = 0;
    virtual void logResult(const std::vector<CelestialObject>& celObjects) = 0;
    virtual void logIter(const std::vector<CelestialObject>& celObjects) = 0;
    virtual void logObject(const CelestialObject& celObject) = 0;
protected:
    long long currentIter = 0;
};