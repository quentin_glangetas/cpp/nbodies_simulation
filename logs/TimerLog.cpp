//
// Created by Quentin Glangetas on 21/10/2023.
//

#include "TimerLog.h"

void TimerLog::logInitialization(const std::vector<CelestialObject> &celObjects) {

}

void TimerLog::logResult(const std::vector<CelestialObject> &celObjects) {
    double timerMeanMs = calculateVectorMean(iterTimes);
    double timerMeanSec = timerMeanMs / 1000;
    double timerStdMs = calculateVectorStd(iterTimes);

    std::cout << std::setprecision(2) << timerMeanMs << " ± " << timerStdMs << " ms/iter" << std::endl;
    std::cout << (int)(1 / timerMeanSec) << " iter/s" << std::endl;
}

void TimerLog::logIter(const std::vector<CelestialObject> &celObjects) {
    timePoint now = std::chrono::high_resolution_clock::now();
    if (hasClockStarted) {
        std::chrono::duration<double, std::milli> msDuration = now - previousIterTime;
        iterTimes.emplace_back(msDuration.count());
    }
    previousIterTime = now;
    hasClockStarted = true;
}

void TimerLog::logObject(const CelestialObject &celObject) {

}
