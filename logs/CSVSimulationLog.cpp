//
// Created by Quentin Glangetas on 01/10/2023.
//

#include "CSVSimulationLog.h"


CSVSimulationLog::CSVSimulationLog(std::string filePath) : filePath(std::move(filePath)), fileStream() {
    fileStream.open(this->filePath, std::ofstream::out | std::ofstream::trunc);
}

void CSVSimulationLog::logInitialization(const std::vector<CelestialObject> &celObjects) {
    fileStream << getHeaders();

    for (const auto& object: celObjects) {
        logObject(object);
    }

}

void CSVSimulationLog::logResult(const std::vector<CelestialObject> &celObjects) {

}

void CSVSimulationLog::logIter(const std::vector<CelestialObject> &celObjects) {
    currentIter++;

    for (const auto& object: celObjects) {
        logObject(object);
    }

}

void CSVSimulationLog::logObject(const CelestialObject &celObject) {
    fileStream << currentIter << ","
               << &celObject << ","
               << getCelestialObjectTypeVerbose(celObject.getType()) << ","
               << celObject.getPosition().x << ","
               << celObject.getPosition().y << ","
               << celObject.getSpeed().x << ","
               << celObject.getSpeed().y << "\n";
}

inline std::string CSVSimulationLog::getHeaders() {
    return "iter,id,type,pos_x,pos_y,speed_x,speed_y\n";
}

CSVSimulationLog::~CSVSimulationLog() {
    fileStream.flush();
    fileStream.close();
}

std::string CSVSimulationLog::getCelestialObjectTypeVerbose(const CelestialType& type) {
    switch (type) {
        case STAR: return "star";
        case PLANET: return "planet";
        case ASTEROID: return "asteroid";
        default: return "unknown";
    }
}
