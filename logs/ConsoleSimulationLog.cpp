//
// Created by Quentin Glangetas on 01/10/2023.
//

#include "ConsoleSimulationLog.h"

void ConsoleSimulationLog::logInitialization(const std::vector<CelestialObject> &celObjects) {
    std::cout << "*** Initialization ***\n";

    for (const auto& object: celObjects) {
        logObject(object);
    }
}

void ConsoleSimulationLog::logResult(const std::vector<CelestialObject> &celObjects) {
    std::cout << "*** Final ***\n";

    for (const auto& object: celObjects) {
        logObject(object);
    }
}

void ConsoleSimulationLog::logIter(const std::vector<CelestialObject> &celObjects) {
    currentIter++;

    if (currentIter % nIter == 0) {
        std::cout << "*** Iter " << currentIter << " ***\n";

        for (const auto& object: celObjects) {
            logObject(object);
        }
    }
}

void ConsoleSimulationLog::logObject(const CelestialObject &celObject) {
    std::cout << "CelestialObject("
              << "id=" << &celObject << ", "
              << "x=" << celObject.getPosition().x << ", "
              << "y=" << celObject.getPosition().y << ", "
              << "dx=" << celObject.getSpeed().x << ", "
              << "dy=" << celObject.getSpeed().y << ")\n";
}
