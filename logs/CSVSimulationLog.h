//
// Created by Quentin Glangetas on 01/10/2023.
//

#pragma once

#include "fstream"
#include <string>
#include "SimulationLog.h"

class CSVSimulationLog : public SimulationLog {
public:
    explicit CSVSimulationLog(std::string  filePath);
    ~CSVSimulationLog() override;

    void logInitialization(const std::vector<CelestialObject> &celObjects) override;
    void logResult(const std::vector<CelestialObject> &celObjects) override;
    void logIter(const std::vector<CelestialObject> &celObjects) override;
    void logObject(const CelestialObject &celObject) override;

protected:
    const std::string filePath;
    std::ofstream fileStream;

    static inline std::string getHeaders();
    static inline std::string getCelestialObjectTypeVerbose(const CelestialType& type);
};
