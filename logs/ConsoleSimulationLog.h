//
// Created by Quentin Glangetas on 01/10/2023.
//

#pragma once

#include <iostream>
#include "SimulationLog.h"

class ConsoleSimulationLog : public SimulationLog {
public:
    explicit ConsoleSimulationLog(uint32_t nIter = 1) : nIter(nIter) {}
    ~ConsoleSimulationLog() override = default;

    void logInitialization(const std::vector<CelestialObject> &celObjects) override;
    void logResult(const std::vector<CelestialObject> &celObjects) override;
    void logIter(const std::vector<CelestialObject> &celObjects) override;
    void logObject(const CelestialObject &celObject) override;

private:
    uint32_t nIter;
};
