//
// Created by Quentin Glangetas on 01/10/2023.
//

#pragma once

#include <vector>
#include "SimulationLog.h"

class MultiSimulationLog : public SimulationLog {
public:
    MultiSimulationLog() = default;
    explicit MultiSimulationLog(std::vector<SimulationLog*>& logPtrs) : logs(logPtrs) {}
    explicit MultiSimulationLog(std::vector<SimulationLog>& simulationLogs);
    void addLog(SimulationLog* log) { logs.push_back(log); }
    ~MultiSimulationLog() override;

    void logInitialization(const std::vector<CelestialObject> &celObjects) override;
    void logResult(const std::vector<CelestialObject> &celObjects) override;
    void logIter(const std::vector<CelestialObject> &celObjects) override;
    void logObject(const CelestialObject &celObject) override;

protected:
    std::vector<SimulationLog*> logs;
};
