//
// Created by Quentin Glangetas on 27/09/2023.
//

#pragma once

struct consts {
    constexpr static const double G = 6.67430e-11;
    constexpr static const double PI = 3.141592;
    constexpr static const uint32_t averagePlanetPerStar = 5;
    constexpr static const uint32_t averageSatellitePerPlanet = 2;

};