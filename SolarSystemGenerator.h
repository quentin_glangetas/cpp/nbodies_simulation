//
// Created by Quentin Glangetas on 21/10/2023.
//

#pragma once

#include <random>
#include <vector>
#include <stdexcept>
#include "constants.h"
#include "Vec2d.h"
#include "CelestialObject.h"


using AngleDistribution = std::uniform_real_distribution<double>;
using MassDistribution = std::uniform_real_distribution<double>;
using DistanceDistribution = std::uniform_real_distribution<double>;

class SolarSystemGenerator {
public:
    SolarSystemGenerator(int nAsteroids, int nPlanets = 8);
    const std::vector<CelestialObject> &getCelObjects() const;
private:
    std::vector<CelestialObject> celObjects;
    mutable std::mt19937 randomGenerator;
    [[nodiscard]] inline Vec2d generateAsteroidPosition() const;
    [[nodiscard]] inline double generateAsteroidMass() const;
    [[nodiscard]] inline double generateAsteroidDistance() const;
    [[nodiscard]] inline double generateAngle() const;
    [[nodiscard]] inline CelestialObject generateAsteroid() const;
};
