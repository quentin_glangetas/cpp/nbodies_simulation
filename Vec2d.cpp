//
// Created by Quentin Glangetas on 25/09/2023.
//

#include "Vec2d.h"

double Vec2d::getNorm() const {
    return sqrt(getNorm2());
}

double Vec2d::getNorm2() const {
    return x * x + y * y;
}

bool Vec2d::operator==(const Vec2d& other) const {
    return (x == other.x) && (y == other.y);
}

Vec2d &Vec2d::operator+=(const Vec2d &other) {
    x += other.x;
    y += other.y;
    return *this;
}

Vec2d &Vec2d::operator-=(const Vec2d &other) {
    x -= other.x;
    y -= other.y;
    return *this;
}

Vec2d &Vec2d::operator*=(double multiplier) {
    x *= multiplier;
    y *= multiplier;
    return *this;
}

Vec2d &Vec2d::operator/=(double divisor) {
    if (divisor == 0) {
        throw std::domain_error("Division by zero not allowed");
    }
    x /= divisor;
    y /= divisor;
    return *this;
}

Vec2d Vec2d::operator+(const Vec2d &rhs) const {
    return { x + rhs.x, y + rhs.y };
}

Vec2d Vec2d::operator-(const Vec2d &rhs) const {
    return { x - rhs.x, y - rhs.y };
}

Vec2d Vec2d::operator*(double multiplier) const {
    return {multiplier * x, multiplier * y};
}

Vec2d Vec2d::operator/(double divisor) const {
    if (divisor == 0) {
        throw std::domain_error("Division by zero not allowed");
    }
    return {x / divisor, y / divisor};
}

Vec2d Vec2d::getVectorTowards(const Vec2d &destination) const {
    return {destination.x - x, destination.y - y};
}

Vec2d Vec2d::getVectorFrom(const Vec2d &source) const {
    return {x - source.x, y - source.y};
}

Vec2d Vec2d::getUnitVectorTowards(const Vec2d &destination) const {
    Vec2d vec = getVectorTowards(destination);
    vec /= vec.getNorm();
    return vec;
}

Vec2d Vec2d::getUnitVectorFrom(const Vec2d &source) const {
    Vec2d vec = getVectorFrom(source);
    vec /= vec.getNorm();
    return vec;
}

Vec2d Vec2d::getOrthogonalVector(const bool isClockwise) const {
    if (isClockwise)
        return {y, -x};
    else
        return {-y, x};
}

Vec2d &Vec2d::rotate(double angle) {
    double xTemp = cos(angle) * x - sin(angle) * y;
    y = sin(angle) * x + cos(angle) * y;
    x = xTemp;
    return *this;
}

Vec2d Vec2d::rotate(double angle) const {
    return {cos(angle) * x - sin(angle) * y, sin(angle) * x + cos(angle) * y};
}

Vec2d Vec2d::operator-() const {
    return {-x, -y};
}
