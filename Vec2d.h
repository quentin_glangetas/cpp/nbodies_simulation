//
// Created by Quentin Glangetas on 25/09/2023.
//

#pragma once

#include <stdexcept>
#include <cmath>

struct Vec2d {
    double x;
    double y;

    [[nodiscard]] double getNorm() const;
    [[nodiscard]] double getNorm2() const;

    Vec2d() : x(0), y(0) {}
    Vec2d(const double& x, const double& y) : x(x), y(y) {}
    Vec2d(const Vec2d& other) = default;
    bool operator==(const Vec2d& other) const;

    Vec2d& operator+=(const Vec2d& other);
    Vec2d& operator-=(const Vec2d& other);
    Vec2d& operator*=(double multiplier);
    Vec2d& operator/=(double divisor);

    Vec2d operator+(const Vec2d& rhs) const;
    Vec2d operator-(const Vec2d& rhs) const;
    Vec2d operator-() const;
    Vec2d operator*(double multiplier) const;
    Vec2d operator/(double divisor) const;

    [[nodiscard]] Vec2d getVectorTowards(const Vec2d& destination) const;
    [[nodiscard]] Vec2d getVectorFrom(const Vec2d& source) const;
    [[nodiscard]] Vec2d getUnitVectorTowards(const Vec2d& destination) const;
    [[nodiscard]] Vec2d getUnitVectorFrom(const Vec2d& source) const;

    [[nodiscard]] Vec2d getOrthogonalVector(bool isClockwise) const;

    Vec2d& rotate(double angle);
    [[nodiscard]] Vec2d rotate(double angle) const;

};

