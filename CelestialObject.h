//
// Created by Quentin Glangetas on 25/09/2023.
//

#pragma once

#include <cmath>
#include <string>
#include <utility>
#include <stdexcept>
#include "Vec2d.h"
#include "constants.h"

enum CelestialType {
    ANY, STAR, PLANET, ASTEROID, N_TYPES
};

class CelestialObject {
public:
    // Main constructor
    CelestialObject(double mass, const Vec2d& initialPosition, const Vec2d& initialSpeed, const CelestialType type)
    : type(type),
      mass(mass),
      position(initialPosition),
      speed(initialSpeed) {}
    // Keep referenceObject as null
    CelestialObject(double mass, const Vec2d& initialPosition)
        : CelestialObject(mass, initialPosition, Vec2d(), CelestialType::ANY) {}
    CelestialObject(double mass, const Vec2d& initialPosition, CelestialType type)
        : CelestialObject(mass, initialPosition, Vec2d(), type) {}
    CelestialObject(double mass, const Vec2d& initialPosition, const Vec2d& initialSpeed)
        : CelestialObject(mass, initialPosition, initialSpeed, CelestialType::ANY) {}

    void updateObject(Vec2d& force, double dt);

    CelestialObject(const CelestialObject&) = default;
    [[nodiscard]] Vec2d getVectorTowards(const CelestialObject& destination) const;
    [[nodiscard]] Vec2d getVectorFrom(const CelestialObject& source) const;
    [[nodiscard]] Vec2d getUnitVectorTowards(const CelestialObject& destination) const;
    [[nodiscard]] Vec2d getUnitVectorFrom(const CelestialObject& source) const;
    void makeObjectOrbitAround(const CelestialObject& massiveObject, bool isClockwise = true);
    void makeObjectOrbitAround(const CelestialObject&&, bool) = delete; // don't accept rvalue

    [[nodiscard]] CelestialType getType() const;
    [[nodiscard]] double getMass() const;
    [[nodiscard]] const Vec2d &getPosition() const;
    [[nodiscard]] const Vec2d &getSpeed() const;

private:
    const CelestialType type;
    const double mass;
    Vec2d position;
    Vec2d speed;
};
