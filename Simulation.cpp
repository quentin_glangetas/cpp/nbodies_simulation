//
// Created by Quentin Glangetas on 25/09/2023.
//

#include "Simulation.h"


Simulation::Simulation(std::vector<CelestialObject> &celObjects, ForceCalculator& fc, SimulationLog& log) :
    celObjects(celObjects), fc(fc), log(log) {}

void Simulation::simulate(const long long int nIter, double timeIncrement) {
    log.logInitialization(celObjects);

    for (long long iter = 0; iter < nIter; iter++) {
        doIter(timeIncrement);
        log.logIter(celObjects);
    }

    log.logResult(celObjects);
}

void Simulation::doIter(double timeIncrement) {
    Vec2d force;
    for (CelestialObject& mainObject : celObjects) {
        force = fc.calculateForce(mainObject);
        mainObject.updateObject(force, timeIncrement);
    }
}
