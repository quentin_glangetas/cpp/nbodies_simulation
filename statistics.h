//
// Created by Quentin Glangetas on 21/10/2023.
//

#pragma once

#include <cmath>
#include <stdexcept>
#include <vector>

template <typename T>
T calculateVectorMean(const std::vector<T>& vec, const T& initialValue = 0) {
    if (vec.size() == 0) {
        return initialValue;
    }

    T sum = initialValue;

    for (T t : vec) {
        sum += t;
    }

    return sum / vec.size();
}

template <typename T>
T calculateVectorStd(const std::vector<T>& vec, const T& initialValue = 0) {
    if (vec.size() <= 1) {
        throw std::runtime_error("Unable to compute std of vector of sizes (0, 1)");
    }

    T sumSquared = initialValue;
    T mean = calculateVectorMean(vec, initialValue);
    T tmp = initialValue;

    for (T t: vec) {
        tmp = (t - mean);
        sumSquared += tmp * tmp;
    }

    return sqrt(sumSquared / vec.size());
}

